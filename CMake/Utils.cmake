### Debug helpers


### Enable  verbose mode
set(EB_DEBUG_BUILD_SYSTEM OFF CACHE BOOL "Enable verbose output from the build system")


### MACRO eb_message
### Output a message
macro(eb_message msg)
	message(STATUS "* " ${msg})
endmacro()


### MACRO: eb_newline
### Output a build message newline
macro(eb_newline)
	message(STATUS "*")
endmacro()


### MACRO: eb_split
### Output a build message split
macro(eb_split)
	message(STATUS "********************************************************************************")
endmacro()


### MACRO: eb_debug_message
### Output a debug message (only visible if EB_DEBUG_BUILD_SYSTEM is set) 
macro(eb_debug_message msg)
	if(EB_DEBUG_BUILD_SYSTEM)
		eb_message(${msg})
	endif()
endmacro()


### MACRO: eb_debug_variable
### Output the content of a variable as a debug message (only visible if EB_DEBUG_BUILD_SYSTEM is set) 
macro(eb_debug_variable var)
	eb_debug_message("${var} = ${${var}}")
endmacro()