##################################################
## Project configuration
##################################################

# Include the version info header
include(${PROJ_PATH}/VERSION)

# Git info
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${BASE_PATH}/CMake/Modules)
include(GetGitRevisionDescription)
get_git_head_revision(EB_PROJECT_GIT_BRANCH EB_PROJECT_GIT_SHA)

# Fix the branch name (we assume gitflow usage)
string(SUBSTRING "${EB_PROJECT_GIT_BRANCH}" 11 -1 EB_PROJECT_GIT_BRANCH) # 11: strlen /refs/heads/

# Timestamp
string(TIMESTAMP EB_BUILD_TIME "%Y-%m-%d %H:%M UTC" UTC)

# Generate the 'rev' version from the git SHA
string(SUBSTRING "${EB_PROJECT_GIT_SHA}" 0 7 EB_PROJECT_VERSION_REV) # 7 characters from the SHA is git's default abbreviated commit desc

# Create EasyBuild's build info file
configure_file("${BASE_PATH}/CMake/Config/BuildInfo.json.in" "${BIN_PATH}/BuildInfo.json")
