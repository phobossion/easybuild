# This variable will collect all the doxygen-enabled projects
set(EB_DOXYGEN_PATHS CACHE INTERNAL "")

### Configure time automation

### MACRO: eb_add_subdirectory
### Helper macro to call CMake's add_subdirectory and also add the directory in question
### to and internal cache to make it possible to manually add some order-bound folders
### and then just call pl_add_subdir_projects to add the rest automatically
### 	'dir'	Subdir name
macro(eb_add_subdirectory dir)
	add_subdirectory(${dir})

	set(EB_MANUAL_SUBDIRS ${EB_MANUAL_SUBDIRS} ${dir})
endmacro()

### MACRO: pl_exclude_subdirectory
### Helper macro to exclude specific projects from the build while still keeping the
### ability to use pl_add_subdir_projects to automatically collect the rest
###   	'dir' Subdir name
macro(eb_exclude_subdirectory dir)
	set(EB_MANUAL_SUBDIRS ${EB_MANUAL_SUBDIRS} ${dir})
endmacro()

### MACRO: eb_add_subdir_projects
### Searches recursively through all the subdirectories of the current directory
### and looks for all CMake projects, adding them into the build
macro(eb_add_subdir_projects)

	# First collect a list of all subdirectories containing the CMake config file
	set(dir_list "")
	file(GLOB_RECURSE new_list CMakeLists.txt)
	foreach(file ${new_list})
		get_filename_component(dir ${file} PATH)

		# Do not add the current directory
		if(NOT ${dir} STREQUAL ${CMAKE_CURRENT_SOURCE_DIR})

			# Also, we don't support sub-projects
			set(add_it ON)
			#foreach(existing_dir ${dir_list})
			#	if(${dir} MATCHES ${existing_dir})
			#		set(add_it OFF)
			#		break()
			#	endif()
			#endforeach()

			# And we also cannot add the same project multiple times
			foreach(existing_dir ${EB_MANUAL_SUBDIRS})
				if(${dir} MATCHES ${existing_dir})
					set(add_it OFF)
					break()
				endif()
			endforeach()

			if(add_it)
				set(dir_list ${dir_list} ${dir})
			endif()
		endif()
	endforeach()
	list(REMOVE_DUPLICATES dir_list)

	# Now add them as usual
	foreach(dir ${dir_list})
		add_subdirectory(${dir})
	endforeach()
endmacro()

### MACRO: eb_set_current_package
### Set the package name. This will be used for all subsequent project
### definitions until changed
### 	'name' Name of the package
macro(eb_set_current_package name)
	set(EB_CURRENT_PACKAGE ${name})
	set(EB_CURRENT_PACKAGE_ROOT ${CMAKE_CURRENT_SOURCE_DIR})

	eb_newline()
	eb_message("- Package ${name}")
endmacro()

### MACRO: eb_add_include_paths
### Allows to add additional include paths into the current project
### using a list of values
macro(eb_add_include_paths)
	foreach(path ${ARGN})
		set(EB_CURRENT_INCLUDES ${EB_CURRENT_INCLUDES} ${path})
	endforeach()
endmacro()

### MACRO: eb_add_link_libs
### Allows to add additional system libraries to link into the current
### project using a list of values
macro(eb_add_link_libs)
	foreach(path ${ARGN})
		set(EB_CURRENT_LIBS ${EB_CURRENT_LIBS} ${path})
	endforeach()
endmacro()

### MACRO: eb_add_compile_defs
## Allows to add additional compile definitions into the current
## project using a list of values
macro(eb_add_compile_defs)
	foreach(path ${ARGN})
		set(EB_CURRENT_COMPILE_DEFS ${EB_CURRENT_COMPILE_DEFS} ${path})
	endforeach()
endmacro()

### MACRO: eb_remove_compile_defs
### Allows to remove compile definitions from the current
### project using a list of values
macro(eb_remove_compile_defs)
	foreach(path ${ARGN})
		list(REMOVE_ITEM EB_CURRENT_COMPILE_DEFS ${path})
	endforeach()
endmacro()

### MACRO: eb_add_dependencies
### Allows to add additional dependencies into the current
### project using a list of values
macro(eb_add_dependencies)
	foreach(path ${ARGN})
		set(EB_CURRENT_DEPENDENCIES ${EB_CURRENT_DEPENDENCIES} ${path})
	endforeach()
endmacro()

### MACRO: eb_project
### Define new EasyBuild project (executable or library)
### 	'name'	Name of the project
macro(eb_project name)
	
	# Define local project variables
	set(EB_CURRENT_TARGET_NAME ${name})
	set(EB_CURRENT_OUTPUT_NAME "${EB_CURRENT_PACKAGE}.${name}")

	if(NOT "${PROJECT_NAME}" STREQUAL "")
		set(EB_CURRENT_OUTPUT_NAME "${PROJECT_NAME}.${EB_CURRENT_OUTPUT_NAME}")
	endif()
	
	eb_message("  - ${EB_CURRENT_OUTPUT_NAME}")

	# The path in IDE
	string(REPLACE "${EB_CURRENT_PACKAGE_ROOT}" "" EB_CURRENT_IDE_PATH "${CMAKE_CURRENT_SOURCE_DIR}")
	set(EB_CURRENT_IDE_PATH ${EB_CURRENT_PACKAGE}${EB_CURRENT_IDE_PATH}) # There is now a slash at the beginning of EB_CURRENT_IDE_PATH
	get_filename_component(EB_CURRENT_IDE_PATH ${EB_CURRENT_IDE_PATH} DIRECTORY)

	# Begin project
	project(${EB_CURRENT_TARGET_NAME} C CXX ${EB_RC_COMPILER})

	# Parse special flags
	set(no_reset OFF)
	foreach(flag ${ARGN})
		if("${flag}" STREQUAL "NORESET")	# Don't reset this project's lists
			set(no_reset ON)				# Useful for creating multiple projects from the same source
		endif()
	endforeach()

	# Initialize project lists
	if(NOT no_reset)
		set(EB_CURRENT_SOURCES "")
		set(EB_CURRENT_INCLUDES "")
		set(EB_CURRENT_LIBS "")
		set(EB_CURRENT_COMPILE_DEFS "")
		set(EB_CURRENT_DEPENDENCIES "")

		# Set build directories (external projects use this)
		set(EB_CURRENT_DOWNLOAD_DIR ${CMAKE_CURRENT_BINARY_DIR}/Download)
		set(EB_CURRENT_SRC_DIR ${CMAKE_CURRENT_BINARY_DIR}/Source)
		set(EB_CURRENT_BUILD_DIR ${CMAKE_CURRENT_BINARY_DIR}/Build)
		set(EB_CURRENT_INSTALL_DIR ${CMAKE_CURRENT_BINARY_DIR}/Install)
		set(EB_CURRENT_TMP_DIR ${CMAKE_CURRENT_BINARY_DIR}/Temp)
		set(EB_CURRENT_STAMP_DIR ${CMAKE_CURRENT_BINARY_DIR}/Stamp)

		# Set default include paths
		eb_add_include_paths(
			${CMAKE_CURRENT_SOURCE_DIR}/Public
			${CMAKE_CURRENT_SOURCE_DIR}/Private
			${EB_CONFIG_FILE_LOCATION})

		# Set default compile defs
		if(WIN32)
			eb_add_compile_defs(${WIN32_COMPILE_DEFS})
		else()
			eb_add_compile_defs(${LINUX_COMPILE_DEFS})
		endif()

		# Current project's include path
		set(EB_CURRENT_TARGET_INCLUDE_DIRS ${EB_CURRENT_TARGET_NAME}_INCLUDE_DIRS)
		# Default value
		set(${EB_CURRENT_TARGET_INCLUDE_DIRS} ${CMAKE_CURRENT_SOURCE_DIR}/Public CACHE INTERNAL "")

		# Current project's output link libraries
		set(EB_CURRENT_TARGET_LIBRARIES ${EB_CURRENT_TARGET_NAME}_LIBRARIES)
	else()

		# We have to copy setup from the previous project, but update with the new target name
		# Current project's include path
		set(EB_CURRENT_TARGET_INCLUDE_DIRS ${EB_CURRENT_TARGET_NAME}_INCLUDE_DIRS)
		# Default value
		set(${EB_CURRENT_TARGET_INCLUDE_DIRS} ${CMAKE_CURRENT_SOURCE_DIR}/Public CACHE INTERNAL "")

		# Current project's output link libraries
		set(EB_CURRENT_TARGET_LIBRARIES ${EB_CURRENT_TARGET_NAME}_LIBRARIES)
	endif()

endmacro()

### MACRO: eb_collect_source_files
### Collect all source files for the project automatically
### You can specify filters for feature specific
### source files in the parameter list
### For example, defining a folder named /+Zip/ with zip-related
### files inside and passing 'Zip' into this function will include
### all files inside that folder into the project
macro(eb_collect_source_files)

	# First collect all files recursively
	file(GLOB_RECURSE all_files *.h *.hpp *.inl *.c *.cpp *.rc)

	# Filter out duplicities
	list(REMOVE_DUPLICATES all_files)

	# Filter out files not belonging to the current feature set & look for precompiled header
	set(pch_file "")
	foreach(file ${all_files})
		if(file MATCHES "/\\+")

			# Feature specific folder
			set(file_in ON)

			# Platforms are identified by a list of unsupported(!) ones
			foreach(platform ${EB_UNSUPPORTED_PLATFORMS})
				if(file MATCHES "/\\+${platform}")
					list(REMOVE_ITEM all_files ${file})
					set(file_in OFF)
					break()
				endif()
			endforeach()

			if(file_in)

				# Remove supported platform filters out of the file name before processing features
				set(file_name ${file})
				foreach(platform ${EB_SUPPORTED_PLATFORMS})
					string(REGEX REPLACE "/\\+${platform}/" "" file_name ${file_name})
				endforeach()

				# If the filename no longer contains any feature identifiers, keep it
				if(file_name MATCHES "/\\+")
					
					# Let's browse through all the features the user wants to enable and look
					# if this file belong to one of them
					set(keep_file OFF)
					foreach(feature ${ARGN})
						if(file_name MATCHES "/\\+${feature}/")
							set(keep_file ON)
							break()
						endif()
					endforeach()

					if(NOT keep_file)
						list(REMOVE_ITEM all_files ${file})
					endif()
				endif()
			endif()
		elseif(file MATCHES "${EB_CURRENT_TARGET_NAME}.PCH.cpp")
			set(pch_file ${file})
			list(REMOVE_ITEM all_files ${file})
		endif()
	endforeach()
	
	# Handle precompiled header
	if (MSVC AND NOT "${pch_file}" STREQUAL "")
		set(pch_bin "$(IntDir)/${EB_CURRENT_TARGET_NAME}.PCH.pch")
		
		set_source_files_properties(${pch_file}
									PROPERTIES COMPILE_FLAGS "/Yc${EB_CURRENT_TARGET_NAME}.PCH.h /Fp${pch_bin}"
											   OBJECT_OUTPUTS "${pch_bin}")
		set_source_files_properties(${all_files}
									PROPERTIES COMPILE_FLAGS "/Yu\"${EB_CURRENT_TARGET_NAME}.PCH.h\" /FI\"${EB_CURRENT_TARGET_NAME}.PCH.h\" /Fp\"${pch_bin}\""
											   OBJECT_DEPENDS "${pch_bin}")  
		
		# Add precompiled header to sources
		list(APPEND all_files ${pch_file})
	endif()

	# Set the list
	set(EB_CURRENT_SOURCES ${all_files})
endmacro()

### MACRO: eb_group_sources
### Group source files for the current project into IDE folders
macro(eb_group_sources)
	foreach(file ${EB_CURRENT_SOURCES})

		# Retrieve the file path
		get_filename_component(path ${file} PATH)

		# Helper macro
		macro(group_sources_helper dir_path subdir)
			# Strip the filesystem path and keep just the relative path from the current CMakeLists.txt
			string(REGEX MATCH "${CMAKE_CURRENT_SOURCE_DIR}/${subdir}/" path_match ${dir_path})
			if(path_match)
				string(REPLACE ${path_match} "" out_path ${dir_path})
				
				# Merge platform and feature files with their generic siblings
				#[TODO] This does not work properly with subfolders of feature folders
				string(REGEX REPLACE "/\\+(.+)" "" out_path ${out_path})

				# Group name expects '\' as delimiter
				string(REPLACE "/" "\\" out_path ${out_path})

				source_group(${out_path} FILES ${file})
			endif()
		endmacro()

		# Source files can be in Public or Private subfolders in the current source dir
		group_sources_helper(${path} "Public")
		group_sources_helper(${path} "Private")
	endforeach()
endmacro()

### MACRO: pe_build_library
### Build the current project as library
macro(eb_build_library)

	# [TODO] Monolythic build with static libraries
	set(lib_type "SHARED")

	# Set/unset build options for shared libraries (only required for MS Windows, but we
	# set at least the definitions to be able to use those definitions within source
	# codes -> we just use the MS Windows names so that we don't have to invent a
	# new definition for this purpose)
	if("${lib_type}" STREQUAL "SHARED")
		eb_add_compile_defs(_WINDLL _USRDLL)
	else()
		eb_remove_compile_defs(_WINDLL _USRDLL)
	endif()

	# Add the library to build
	add_library(${EB_CURRENT_TARGET_NAME} ${lib_type} ${EB_CURRENT_SOURCES})

	# Generate folder structure in IDE
	eb_group_sources()

	# Includes
	include_directories(${EB_CURRENT_TARGET_NAME} ${EB_CURRENT_INCLUDES})

	# Link libraries
	target_link_libraries(${EB_CURRENT_TARGET_NAME} ${EB_CURRENT_LIBS})

	# Dependencies
	if(NOT "${EB_CURRENT_DEPENDENCIES}" STREQUAL "")
		add_dependencies(${EB_CURRENT_TARGET_NAME} ${EB_CURRENT_DEPENDENCIES})
	endif()

	# Special dependency for the config target
	add_dependencies(${EB_CURRENT_TARGET_NAME} CONFIGURE)

	# Preprocessor definitions
	if(MSVC)
		eb_add_compile_defs(USE_PRECOMPILED_HEADER)
	endif()
	set_target_properties(
		${EB_CURRENT_TARGET_NAME}
		#PROPERTIES COMPILE_DEFINITIONS "${EB_CURRENT_COMPILE_DEFS} $<$<OR:$<CONFIG:Debug>,$<CONFIG:RelWithDbgInfo>>:DEBUG _DEBUG> $<$<OR:$<CONFIG:Release>,$<CONFIG:MinSizeRel>>:NDEBUG>")
		PROPERTIES COMPILE_DEFINITIONS "${EB_CURRENT_COMPILE_DEFS}")

	# Solution folders
	set_property(
		TARGET ${EB_CURRENT_TARGET_NAME}
		PROPERTY FOLDER "${EB_CURRENT_IDE_PATH}")

	# Output file
	set_property(
		TARGET ${EB_CURRENT_TARGET_NAME}
		PROPERTY OUTPUT_NAME ${EB_CURRENT_OUTPUT_NAME})

	# Input libraries for other projects
	set(${EB_CURRENT_TARGET_NAME}_LIBRARIES ${EB_CURRENT_TARGET_NAME} CACHE INTERNAL "")

	# Installation
	# Install runtime and link libraries
	install(
		TARGETS ${EB_CURRENT_TARGET_NAME}
		RUNTIME DESTINATION ${EB_BIN_DIR} COMPONENT Runtime
		LIBRARY DESTINATION ${EB_BIN_DIR} COMPONENT Runtime
		ARCHIVE DESTINATION ${EB_LIB_DIR} COMPONENT SDK
	)

	# Install include directory
	install(
		DIRECTORY ${${EB_CURRENT_TARGET_INCLUDE_DIRS}}/${EB_CURRENT_TARGET_NAME}
		DESTINATION ${EB_INCLUDE_DIR}
	)

endmacro()

### MACRO: eb_build_executable
### Build the current project as executable
### 	'subsystem'	Subsystem for which to build (CONSOLE or WIN32) - ignored on Linux
macro(eb_build_executable subsystem)

	# Check subsystem type on Windows
	# [TODO] Support windowed applications
	if(WIN32)
		# Set subsystem type
		if("${subsystem}" STREQUAL "WIN32")
			set(subsys "WIN32")
		else()
			set(subsys "")
		endif()

		# Add build options for subsystem
		if("${subsys}" STREQUAL "WIN32")
			# Windows application
			eb_remove_compile_defs(_CONSOLE)
			#remove_linker_flags(/SUBSYSTEM:CONSOLE)
			eb_add_compile_defs(_WINDOWS)
			#add_linker_flags(/SUBSYSTEM:WINDOWS)
		else()
			# Console application
			eb_remove_compile_defs(_WINDOWS)
			#remove_linker_flags(/SUBSYSTEM:WINDOWS)
			eb_add_compile_defs(_CONSOLE)
			#add_linker_flags   (/SUBSYSTEM:CONSOLE)
		endif()
	endif()

	# Add the library to build
	add_executable(${EB_CURRENT_TARGET_NAME} ${subsys} ${EB_CURRENT_SOURCES})

	# Generate folder structure in IDE
	eb_group_sources()

	# Includes
	include_directories(${EB_CURRENT_TARGET_NAME} ${EB_CURRENT_INCLUDES})

	# Link libraries
	target_link_libraries(${EB_CURRENT_TARGET_NAME} ${EB_CURRENT_LIBS})

	# Dependencies
	if(NOT "${EB_CURRENT_DEPENDENCIES}" STREQUAL "")
		add_dependencies(${EB_CURRENT_TARGET_NAME} ${EB_CURRENT_DEPENDENCIES})
	endif()

	# Special dependency for the config target
	add_dependencies(${EB_CURRENT_TARGET_NAME} CONFIGURE)

	# Preprocessor definitions
	if(MSVC)
		eb_add_compile_defs(USE_PRECOMPILED_HEADER)
	endif()
	set_target_properties(
		${EB_CURRENT_TARGET_NAME}
		#PROPERTIES COMPILE_DEFINITIONS "${EB_CURRENT_COMPILE_DEFS} $<$<OR:$<CONFIG:Debug>,$<CONFIG:RelWithDbgInfo>>:DEBUG _DEBUG> $<$<OR:$<CONFIG:Release>,$<CONFIG:MinSizeRel>>:NDEBUG>")
		PROPERTIES COMPILE_DEFINITIONS "${EB_CURRENT_COMPILE_DEFS}")

	# Solution folders
	set_property(
		TARGET ${EB_CURRENT_TARGET_NAME}
		PROPERTY FOLDER "${EB_CURRENT_IDE_PATH}")

	# Output file
	set_property(
		TARGET ${EB_CURRENT_TARGET_NAME}
		PROPERTY OUTPUT_NAME ${EB_CURRENT_OUTPUT_NAME})

	# Install
	# Install runtime
	install(
		TARGETS ${EB_CURRENT_TARGET_NAME}
		RUNTIME DESTINATION ${EB_BIN_DIR} COMPONENT SDK
	)

endmacro()

### MACRO: eb_build_external
### Helper to finalize the build of an external dependency project
macro(eb_build_external)

	# Solution folders
	set_property(
		TARGET ${EB_CURRENT_TARGET_NAME}
		PROPERTY FOLDER "${EB_CURRENT_PACKAGE}")

	# Dependencies
	if(NOT "${EB_CURRENT_DEPENDENCIES}" STREQUAL "")
		add_dependencies(${EB_CURRENT_TARGET_NAME} ${EB_CURRENT_DEPENDENCIES})
	endif()

endmacro()

### MACRO: eb_add_dependency
### Add new knows dependency to the currently built module
###  	'module'	Name of the module to add
macro(eb_add_dependency module)
	
	# Add include paths to the module
	eb_add_include_paths(${${module}_INCLUDE_DIRS})

	# Add link libraries
	eb_add_link_libs(${${module}_LIBRARIES})

	# Add the dependency
	eb_add_dependencies(${module})
endmacro()