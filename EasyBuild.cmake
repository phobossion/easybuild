### EasyBuild
### CMake based build pipeline


# Suppress warning about the deprecated target property LOCATION
cmake_policy(SET CMP0026 OLD)
# Use new variable expansion policy
cmake_policy(SET CMP0054 NEW)

# Enable solution folders
set_property(GLOBAL PROPERTY USE_FOLDERS ON)


### Base includes 
include(${EB_ROOT}/CMake/Utils.cmake)
include(${EB_ROOT}/CMake/Platform.cmake)
include(${CMAKE_CURRENT_SOURCE_DIR}/VERSION)


### Print out build info
eb_split()
eb_message("${EB_PROJECT_NAME} Build")
eb_newline()
eb_message("Version:\t${EB_PROJECT_NAME} ${EB_PROJECT_VERSION_MAJOR}.${EB_PROJECT_VERSION_MINOR}.${EB_PROJECT_VERSION_PATCH} (${EB_PROJECT_VERSION_RELEASE})")
eb_message("Platform:\t${EB_SUPPORTED_PLATFORMS} ${EB_TARGET_ARCHBITSIZE}")
eb_message("Compiler:\t${CMAKE_CXX_COMPILER_ID}")
eb_newline()
eb_split()


### Output directories

# Install bin
set(EB_BIN_DIR "${CMAKE_INSTALL_PREFIX}/Bin/${EB_TARGET_ARCHBITSIZE}")
# Developer bin
set(EB_DEV_BIN_DIR "${CMAKE_CURRENT_SOURCE_DIR}/_Bin/${EB_TARGET_ARCHBITSIZE}")

# Install lib
set(EB_LIB_DIR "${CMAKE_INSTALL_PREFIX}/Lib/${EB_TARGET_ARCHBITSIZE}")
# Developer lib
set(EB_DEV_LIB_DIR "${CMAKE_CURRENT_SOURCE_DIR}/_Lib/${EB_TARGET_ARCHBITSIZE}")

# Install include
set(EB_INCLUDE_DIR "${CMAKE_INSTALL_PREFIX}/Include")

# Install documentation
set(EB_DOC_DIR "${CMAKE_INSTALL_PREFIX}/Docs")
# Developer documentation
set(EB_DEV_DOC_DIR "${CMAKE_CURRENT_SOURCE_DIR}/_Docs")


if(MSVC)
	set(EB_PLATFORM_OUT_DIR ${EB_DEV_BIN_DIR}/$(Configuration))
	set(EB_PLATFORM_LIB_DIR ${EB_DEV_LIB_DIR}/$(Configuration))
else()
	set(EB_PLATFORM_OUT_DIR ${EB_DEV_BIN_DIR})
	set(EB_PLATFORM_LIB_DIR ${EB_DEV_LIB_DIR})
endif()

eb_newline()
eb_message("Output directories:")
eb_message("- binaries:\t${EB_PLATFORM_OUT_DIR}")
eb_message("- libraries:\t${EB_PLATFORM_LIB_DIR}")
eb_message("- documentation:\t${EB_DEV_DOC_DIR}")

eb_newline()
eb_message("Install directories:")
eb_message("- binaries:\t${EB_BIN_DIR}")
eb_message("- libraries:\t${EB_LIB_DIR}")
eb_message("- documentation:\t${EB_DOC_DIR}")
eb_newline()
eb_split()

# [TODO]: Do we need the developer build?
#set(EB_DEVELOPER_BUILD ON CACHE BOOL "Build a version suitable for developing Troll itself")

#if(${EB_DEVELOPER_BUILD})
	set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${EB_DEV_LIB_DIR})
	set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${EB_DEV_LIB_DIR})
	set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${EB_DEV_BIN_DIR})
#	set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_DEBUG ${EB_DEV_LIB_DIR})
#	set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_DEBUG ${EB_DEV_LIB_DIR})
#	set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG ${EB_DEV_BIN_DIR})
#	set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_RELEASE ${EB_DEV_LIB_DIR})
#	set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_RELEASE ${EB_DEV_LIB_DIR})
#	set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE ${EB_DEV_BIN_DIR})
#else()
#	set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY)
#	set(CMAKE_LIBRARY_OUTPUT_DIRECTORY)
#	set(CMAKE_RUNTIME_OUTPUT_DIRECTORY)
#	set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_DEBUG)
#	set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_DEBUG)
#	set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG)
#	set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_RELEASE)
#	set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_RELEASE)
#	set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE)
#endif()


### More includes
include(${EB_ROOT}/CMake/Build.cmake)
include(${EB_ROOT}/CMake/Project.cmake)


### Custom config step

# The configuration target
add_custom_target(
	CONFIGURE
	COMMAND ${CMAKE_COMMAND} 	-D BASE_PATH=${EB_ROOT}
                                -D PROJ_PATH=${CMAKE_SOURCE_DIR}
								-D BIN_PATH=${EB_PLATFORM_OUT_DIR}
								-P ${EB_ROOT}/CMake/Config.cmake
	ADD_DEPENDENCIES ${EB_ROOT}/CMake/Config.cmake)
